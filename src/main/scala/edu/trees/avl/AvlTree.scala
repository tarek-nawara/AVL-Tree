package edu.trees.avl


/** Representation of AVL Tree and all the supported
  * operations over them
  *
  * @tparam T type of elements in the tree.
  */
sealed trait AvlTree[+T] {
  type MergeFun[U] = (U, List[U], List[U]) => List[U]

  /**
    * Add an element and return a new AVL tree.
    *
    * @param data element to add
    * @param ord  ordering function
    * @return new AVL Tree with the added element
    */
  def add[U >: T](data: U)(implicit ord: Ordering[U]): AvlTree[U]

  /**
    * Test if the tree contains the given element.
    *
    * @param data data to test its existence
    * @param ord  ordering function
    * @return true if exist, false otherwise
    */
  def contains[U >: T](data: U)(implicit ord: Ordering[U]): Boolean

  /**
    * Traverse the tree inorder.
    *
    * @return list containing all elements in the list
    */
  def traverse[U >: T](): List[U]

  /**
    * Traverse the given tree.
    *
    * @param f merger function
    * @return a list containing all elements in the list
    */
  def traverse[U >: T](f: MergeFun[U]): List[U]

  def size: Int

  def height: Int
}

/** Representation of empty node. */
object Empty extends AvlTree[Nothing] {

  override val size: Int = 0
  override val height: Int = 0

  /**
    * Add an element and return a new AVL tree.
    *
    * @param data element to add
    * @param ord  ordering function
    * @return new AVL Tree with the added element
    */
  override def add[U >: Nothing](data: U)(implicit ord: Ordering[U]): AvlTree[U] = Fork(data, Empty, Empty)

  /**
    * Test if the tree contains the given element.
    *
    * @param data data to test its existence
    * @param ord  ordering function
    * @return true if exist, false otherwise
    */
  override def contains[U >: Nothing](data: U)(implicit ord: Ordering[U]): Boolean = false

  /**
    * Traverse the tree inorder.
    *
    * @return list containing all elements in the list
    */
  override def traverse[U >: Nothing](): List[U] = Nil

  /**
    * Traverse the given tree.
    *
    * @param f merger function
    * @return a list containing all elements in the list
    */
  override def traverse[U >: Nothing](f: MergeFun[U]): List[U] = Nil
}

/** Representation of a fork node. */
case class Fork[T](elem: T, var left: AvlTree[T], var right: AvlTree[T]) extends AvlTree[T] {

  var size: Int = 1 + left.size + right.size
  var height: Int = 1 + math.max(left.height, right.height)

  /**
    * Add an element and return a new AVL tree.
    *
    * @param data element to add
    * @param ord  ordering function
    * @return new AVL Tree with the added element
    */
  override def add[U >: T](data: U)(implicit ord: Ordering[U]): AvlTree[U] = ord.compare(data, elem) match {
    case 0 => balance(this)
    case 1 => balance(Fork(elem, left, right.add(data)))
    case _ => balance(Fork(elem, left.add(data), right))
  }

  /**
    * Test if the tree contains the given element.
    *
    * @param data data to test its existence
    * @param ord  ordering function
    * @return true if exist, false otherwise
    */
  override def contains[U >: T](data: U)(implicit ord: Ordering[U]): Boolean = ord.compare(data, elem) match {
    case 0 => true
    case 1 => right.contains(data)
    case _ => left.contains(data)
  }

  /**
    * Traverse the tree inorder.
    *
    * @return list containing all elements in the list
    */
  override def traverse[U >: T](): List[U] = {
    traverse((e, l, r) => (l :+ e) ::: r)
  }

  /**
    * Traverse the given tree.
    *
    * @param f merger function
    * @return a list containing all elements in the list
    */
  override def traverse[U >: T](f: MergeFun[U]): List[U] = {
    f(elem, left.traverse(f), right.traverse(f))
  }

  /**
    * Balance the given tree by applying left rotation
    * or right rotation if needed.
    *
    * @param tree tree to balance
    * @return balanced tree
    */
  private def balance[U >: T](tree: Fork[U]): Fork[U] = {
    val heightDiff = tree.left.height - tree.right.height
    if (heightDiff > 1) {
      val leftFork = tree.left.asInstanceOf[Fork[T]]
      if (leftFork.left.height >= leftFork.right.height) {
        rightRotate(tree)
      } else {
        tree.left = leftRotate(leftFork)
        rightRotate(tree)
      }
    } else if (heightDiff < -1) {
      val rightFork = tree.right.asInstanceOf[Fork[T]]
      if (rightFork.right.height >= rightFork.left.height) {
        leftRotate(tree)
      } else {
        tree.right = rightRotate(rightFork)
        leftRotate(tree)
      }
    } else {
      tree
    }
  }

  /**
    * Apply right rotation to the given tree.
    *
    * @param root target tree
    * @return rotated tree.
    */
  private def rightRotate[U >: T](root: Fork[U]): Fork[U] = {
    val newRoot = root.left.asInstanceOf[Fork[U]]
    root.left = newRoot.right
    newRoot.right = root
    root.updateHeight()
    newRoot.updateHeight()
    newRoot
  }

  /**
    * Apply left rotation to the given tree.
    *
    * @param root target tree.
    * @return rotated tree.
    */
  private def leftRotate[U >: T](root: Fork[U]): Fork[U] = {
    val newRoot = root.right.asInstanceOf[Fork[U]]
    root.right = newRoot.left
    newRoot.left = root
    root.updateHeight()
    newRoot.updateHeight()
    newRoot
  }

  /**
    * Update the height of the underlying tree.
    */
  private def updateHeight(): Unit = {
    this.height = 1 + math.max(left.height, right.height)
  }
}


object AvlTree {
  def apply(): AvlTree[Nothing] = Empty

  def apply[T](elems: T*)(implicit ord: Ordering[T]): AvlTree[T] = {
    elems.foldLeft(Empty.asInstanceOf[AvlTree[T]])((tree, elem) => tree.add(elem))
  }
}
