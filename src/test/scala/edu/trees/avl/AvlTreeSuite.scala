package edu.trees.avl

import org.scalatest.FunSuite

class AvlTreeSuite extends FunSuite {
  test("sanity test") {
    val tree = Empty
    val other = tree.add(1)
    assert(other.size == 1)
    assert(other.contains(1))
  }

  test("adding elements to the tree") {
    val tree = AvlTree(3, 1, 6, 4, 5)
    val list = tree.traverse()
    assert(list == List(1, 3, 4, 5, 6))
  }
}
